var cheerio = require('cheerio');
var parser = {
  summary: summary,
  details: details,
  owner: owner,
  assesment: assesment,
};
function getValueByKey($, key) {
  return $('strong:contains(' + key + ')').parents('td').next().text().trim();
}
function getValueByHeader($, key, last) {
  var td = $('strong:contains(' + key + ')').parents('td');
  var idx = td.index();
  var tr = last ? td.parents('tr').siblings().last() : td.parents('tr').next();
  return $('td:nth-child(' + (idx + 1) + ')', tr).text().trim();
}
function summary(text) {
  var $ = cheerio.load(text);
  var parcelNumber = getValueByKey($, 'Parcel Number');
  var zone = getValueByKey($, 'Zone');
  var currentOwner = getValueByKey($, 'Current Owner');
  var ownerAddress = getValueByKey($, 'Owner Address');
  var ownerCity = getValueByKey($, 'Owner City State');
  var ownerAddressAll = {
    ownerAddress: ownerAddress,
    ownerCity: ownerCity,
  };
  var schoolZone = getValueByKey($, 'School Zone');
  var acreage = getValueByKey($, 'Acreage');
  var output = {
    'Parcel Number': parcelNumber,
    'Zone': zone,
    'Current Owner': currentOwner,
    'Owner Address (with city and state)': ownerAddressAll,
    'School Zone': schoolZone,
    'Acreage': acreage,
  };
  return output;
}
function details(text) {
  var $ = cheerio.load(text);
  var sqftFinishedLiving = getValueByKey($, 'SqFt Finished Living');
  var yearBuilt = getValueByKey($, 'YearBuilt');
  var bedrooms = getValueByKey($, 'Bedrooms');
  var output = {
    'SqFt Finished Living': sqftFinishedLiving,
    'YearBuilt': yearBuilt,
    'Bedrooms': bedrooms,
  };
  return output;
}
function owner(text) {
  var $ = cheerio.load(text);
  var dateOfLastSale = getValueByHeader($, 'Date of Sale', true);
  var salePrice = getValueByHeader($, 'Sale Price');
  var output = {
    'Date of Last Sale': dateOfLastSale,
    'Sale Price': salePrice,
  };
  return output;
}
function assesment(text) {
  var $ = cheerio.load(text);
  var landValue = getValueByHeader($, 'Land Value');
  var ImprovementValue = getValueByHeader($, 'Improvement Value');
  var output = {
    '2015 Land Value': landValue,
    '2015 Improvement Value': ImprovementValue,
  };
  return output;
}
module.exports = parser;
