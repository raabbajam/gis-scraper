var request = require('request');
var faker = require('faker');
// var json2xls = require('json2xls');
// var xlsx = require('node-xlsx');
var Promise = require('promise');
var parser = require('./parser');
var methods = ['summary','details','owner','assesment'];

function scraper(key) {
  return scrape(key)
    .then(function (bodies) {
      return bodies.reduce(function (all, body, i) {
        var method = methods[i];
        all[method] = parser[method](body);
        return all;
      }, {});
    })
    // .then(function (json) {
    //   console.log(json);
    //   return getCsv(json);
    // });
}

function scrape(key) {
  var urls = getUrls(key);
  var promises = urls.map(function (url) {
    return new Promise(function(resolve, reject) {
      var options = getOption(url);
      var callback = _callback(resolve, reject);
      return request(options, callback);
    });
  });
  return Promise.all(promises);
}
function getOption(url) {
  return {
    url: url,
    headers: { 'User-Agent': faker.internet.userAgent() }
  };
}
function _callback(resolve, reject) {
  return function (err, response, body) {
    if (err) {
      console.log('error request:', err.stack);
      return reject(err);
    }
    return resolve(body);
  };
}
function getUrls(key) {
  var base = 'http://gisweb.charlottesville.org/GISViewer/ParcelViewer/';
  return [
    base + 'Details' + '?Key=' + key + '&SearchOptionIndex=0&DetailsTabIndex=0',
    base + 'GetDetailTab' + '?Key=' + key + '&SearchOptionIndex=0&DetailsTabIndex=1',
    base + 'GetDetailTab' + '?Key=' + key + '&SearchOptionIndex=0&DetailsTabIndex=2',
    base + 'GetDetailTab' + '?Key=' + key + '&SearchOptionIndex=0&DetailsTabIndex=3',
  ];
}
module.exports = scraper;
