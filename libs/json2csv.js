var jsoncsv = require('json-csv');
var Promise = require('promise');
function getCsv(json) {
  json = json instanceof Array ? json : [json];
  return new Promise(function(resolve, reject) {
    var options = { fields : [
          { name : 'summary.Parcel Number', label : 'Summary - Parcel Number', },
          { name : 'summary.Zone', label : 'Summary - Zone', },
          { name : 'summary.Current Owner', label : 'Summary - Current Owner', },
          { name : 'summary.Owner Address (with city and state).ownerAddress', label : 'Summary - Owner Address', },
          { name : 'summary.Owner Address (with city and state).ownerCity', label : 'Summary - Owner Address City and State', },
          { name : 'summary.School Zone', label : 'Summary - School Zone', },
          { name : 'summary.Acreage', label : 'Summary - Acreage', },
          { name : 'details.SqFt Finished Living', label : 'Details - SqFt Finished Living', },
          { name : 'details.YearBuilt', label : 'Details - YearBuilt', },
          { name : 'details.Bedrooms', label : 'Details - Bedrooms', },
          { name : 'owner.Date of Last Sale', label : 'Owner - Date of Last Sale', },
          { name : 'owner.Sale Price', label : 'Owner - Sale Price', },
          { name : 'assesment.2015 Land Value', label : 'Assesment - 2015 Land Value', },
          { name : 'assesment.2015 Improvement Value', label : 'Assesment - 2015 Improvement Value', },
      ] } ;
    jsoncsv.csvBuffered(json, options, callback);
    function callback(err, csv) {
      if (err) {
        console.log('Error csv:', err.stack);
        return reject(err);
      }
      return resolve(csv);
    }
  });
}
module.exports = getCsv;
