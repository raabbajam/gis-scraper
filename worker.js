var kue = require('kue');
var config = require('./config');
var queue = kue.createQueue({
  prefix: 'gis',
  disableSearch: false,
  redis: config.redis,
});

var scraper = require('./libs/scraper');
var parrallelJob = config.parrallelJob || 3;

var cluster = require('cluster');
var clusterWorkerSize = require('os').cpus().length;
console.log('listening for scrape job on %d fork cpu.', clusterWorkerSize);
if (cluster.isMaster) {
  kue.app.listen(3000);
  for (var i = 0; i < clusterWorkerSize; i++) {
    cluster.fork();
  }
} else {
  queue.process('scrape', parrallelJob, function(job, done){
    console.log('start scraping for ', job.data.id);
    scraper(job.data.id)
      .then(function (json) {
        console.log('finish scraping for ', job.data.id);
        done(null, json);
      })
      .catch(function (err) {
        done(err);
      });
  });
}
