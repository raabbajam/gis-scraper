var kue = require('kue');
var config = require('./config');
console.log(config);
var queue = kue.createQueue({
  prefix: 'gis',
  disableSearch: false,
  redis: config.redis,
});
var fs = require('fs');
var path = require('path');
var read = fs.readFileSync;
var write = fs.writeFileSync;
var wstream = fs.createWriteStream(__dirname + '/myOutput.json');
var Promise = require('bluebird');
var faker = require('faker');
var request = require('request');
var json2csv = require('./libs/json2csv');
// request.debug = true;
var urlCount = 'http://gisweb.charlottesville.org/GISViewer/ParcelViewer/Search';
var urlResult = 'http://gisweb.charlottesville.org/GISViewer/ParcelViewer/Results';
var perPage = config.perPage || 20;
var attempt = config.attempt || 5;
var maxPage = config.maxPage || false;
function gis() {
  var count;
  getCount()
    .then(function (count) {
      console.log('count', count);
      return maxPage || Math.ceil(count / perPage);
      // return 2;
    })
    .then(function (pages) {
      var promises = Promise.resolve();
      var values = [];
      for (var i = 0; i < pages; i++) {
        reducer = _reducer(i);
        promises = promises.then(reducer);
      }
      return promises.then(function (val) {
        values.push(val);
        return values;
      });
      function _reducer(i) {
        return function (val) {
          values.push(val);
          return getIds(i);
        };
      }
    })
    .then(function (ids) {
      return ids.reduce(function (previous, current) {
        return previous.concat(current);
      }, []);
    })
    .then(function (ids) {
      // console.log('ids', ids);
      ids.shift(); // remove first element
      var finished = 0;
      var all = ids.length;
      var json = [];
      wstream.write('[');
      return new Promise(function(resolve, reject) {
        ids.forEach(function (id) {
          // TODO check if id is already scraped
          queue
            .create('scrape', {
                title: 'scrape for id ' + id,
                id: id,
            })
            .attempts(attempt)
            // .searchKeys(['id'])
            .save(function(){})
            .on('complete', function(result){
              json.push(result);
              // console.log(json);
              wstream.write(JSON.stringify(result) + ',\n');
              finished++;
              console.log('complete job %s. progress %d / %d', id, finished, all);
              if (finished >= all) {
                console.log('all is done');
                wstream.write(']');
                wstream.end();
                return resolve(json);
              }
            });
          });
        });
    })
    .then(function (json) {
      return json2csv(json);
    })
    .then(function (csv) {
      write(__dirname + '/myOutput.csv', csv);
      process.exit();
    })
    .catch(function (err) {
      console.log(err.stack);
      process.exit();
    });
}
function getIds(page) {
  console.log('getid page', page);
  return new Promise(function(resolve, reject) {
    var options = {
      url: urlResult,
      method: 'post',
      headers: getHeader(),
      formData: getFormData(page),
    };
    var callback = _callback(resolve, reject);
    return request(options, callback);
  });
}
function getFormData(page) {
  return {
    SrchOptIdx: '0',
    acFullAddress: '',
    propName: '',
    propNum: '',
    propStName: '',
    ddl_ParZONE: '',
    propID: '',
    _search: 'false',
    nd: new Date().valueOf(),
    rows: perPage,
    page: page + 1,
    sidx: 'mapblolot1',
    sord: 'asc',
  };
}
function getHeader() {
  return {
    "Host": "gisweb.charlottesville.org",
    "Connection": "keep-alive",
    "Pragma": "no-cache",
    "Cache-Control": "no-cache",
    "Origin": "http://gisweb.charlottesville.org",
    "User-Agent": faker.internet.userAgent(),
    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    "Accept": "application/json, text/javascript, */*; q=0.01",
    "X-Requested-With": "XMLHttpRequest",
    "X-FirePHP-Version": "0.0.6",
    "Referer": "http://gisweb.charlottesville.org/GISViewer/",
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "en-US,en;q=0.8,ko;q=0.6,id;q=0.4,ms;q=0.2,fr;q=0.2",
    "Cookie": "_ga=GA1.2.958617678.1427883283; _gat=1",
  };
}
function _callback(resolve, reject) {
  return function (err, response, body) {
    if (err) {
      console.log('error request:', err.stack);
      return reject(err);
    }
    var json = parseBody(body);
    return resolve(json);
  };
}
function parseBody(json) {
  json = typeof json === 'string' ? JSON.parse(json) : json;
  return json.rows.map(function (row) {
    return row.cell[6].trim();
  });
}
function getCount() {
  return new Promise(function(resolve, reject) {
    var options = {
      url: urlCount,
      formData: {
        "SrchOptIdx": 0,
        "acFullAddress": '',
        "propName": '',
        "propNum": '',
        "propStName": '',
        "ddl_ParZONE": '',
        "propID": '',
      },
      method: 'POST',
      headers: getHeader(),
    };
    console.log('options');
    request(options, function (err, response, body) {
      if (err) {
        console.log('error request:', err.stack);
        return reject(err);
      }
      var count = JSON.parse(body).count;
      return resolve(count);
    });
  });
}
gis();
