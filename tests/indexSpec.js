var expect = require('chai').expect;
var fs = require('fs');
var read = fs.readFileSync;
var write = function (filename, text) {
  return fs.writeFileSync(filename, JSON.stringify(text, null, 2));
};
// var write = fs.writeFileSync;
var gis = require('../index');
describe('gis', function () {
	this.timeout(50000);
  it('should scrape all pages', function (done) {
    var page = '1';
    gis(page)
      .then(function (json) {
        write(__dirname + '/ids.json', json);
        done();
      })
      .catch(function (err) {
        console.log(err.stack);
        done(err);
      });
  });

});
