var expect = require('chai').expect;
var fs = require('fs');
var read = fs.readFileSync;
var write = function (filename, text) {
  return fs.writeFileSync(filename, JSON.stringify(text, null, 2));
};
var parser = require('../../../libs/parser');
describe('parser libs', function () {
  it('should parse summary page', function (done) {
    var text = read(__dirname + '/summary.html');
    var output = parser.summary(text);
    write(__dirname + '/summary.json', output);
    expect(output).to.exist;
    done();
  });
  it('should parse details page', function (done) {
    var text = read(__dirname + '/details.html');
    var output = parser.details(text);
    write(__dirname + '/details.json', output);
    expect(output).to.exist;
    done();
  });
  it('should parse owner page', function (done) {
    var text = read(__dirname + '/owner.html');
    var output = parser.owner(text);
    write(__dirname + '/owner.json', output);
    expect(output).to.exist;
    done();
  });
  it('should parse assesment page', function (done) {
    var text = read(__dirname + '/assesment.html');
    var output = parser.assesment(text);
    write(__dirname + '/assesment.json', output);
    expect(output).to.exist;
    done();
  });
});
