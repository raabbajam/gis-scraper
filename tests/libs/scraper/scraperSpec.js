var expect = require('chai').expect;
var fs = require('fs');
var read = fs.readFileSync;
// var write = function (filename, text) {
//   return fs.writeFileSync(filename, JSON.stringify(text, null, 2));
// };
var write = fs.writeFileSync;
var scraper = require('../../../libs/scraper');
var json2csv = require('../../../libs/json2csv');

describe('scraper lib', function () {
	this.timeout(50000);
  it('should scrape all pages', function (done) {
    var key = '010001100';
    scraper(key)
      .then(function (json) {
        return json2csv(json);
      })
      .then(function (csv) {
        write(__dirname + '/all.csv', csv);
        done();
      })
      .catch(function (err) {
        console.log(err.stack);
        done(err);
      });
  });

});
