var json2csv = require('../../libs/json2csv');
var fs = require('fs');
var path = require('path');
var read = fs.readFileSync;
var write = fs.writeFileSync;
var text = read(path.resolve(__dirname + '../../../myOutput.json'));
var json = JSON.parse(text);
json2csv(json)
  .then(function (csv) {
    write(path.resolve(__dirname + '../../../myOutput.csv'), csv);
    console.log('All is done');
    process.exit();
  });
